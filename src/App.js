import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import Home from "./pages/Home";
import NavBar from "./components/NavBar";
import SignUpModal from "./components/SignUpModal";
import SignInModal from "./components/SignInModal";
import Private from "./pages/Private/Private";
import PrivateHome from "./pages/Private/PrivateHome/PrivateHome";


import { UserContextProvider } from "./context/userContext";

function App() {
  return (
    <>
    <div style={{height: "100%", color:"red"}}>
      <UserContextProvider>
        <BrowserRouter>
        <SignUpModal /> 
        <SignInModal />
          <NavBar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/private" element={<Private />}>
              <Route path="/private/private-home" element={<PrivateHome />}/>
            </Route>
          </Routes>
        </BrowserRouter>
      </UserContextProvider>
    </div>
      
    </>
  );
}

export default App;
