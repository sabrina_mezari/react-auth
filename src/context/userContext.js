import React,{ createContext, useEffect, useState } from "react";

import {
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    onAuthStateChanged
} from "firebase/auth";

import {auth} from "../firebase-config";

export const UserContext = createContext();

function UserContextProvider({children}) {

    const signUp = (email,pwd) => createUserWithEmailAndPassword(auth, email, pwd);
    const signIn = (email,pwd) => signInWithEmailAndPassword(auth, email, pwd);

    const [currentUser, setCurrentUser] = useState();
    const [loadingData, setLoadingData] = useState(true);

   useEffect(() => {
       const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
           setCurrentUser(currentUser)
           setLoadingData(false)
       })

       return unsubscribe;
   })

    //modal
    const [modalState, setModalState] = useState({
        signUpModal: false,
        signInModal: false,
    });

    const toggleModals = modal => {
        if(modal === "signin") {
            setModalState({
                signUpModal: false,
                signInModal: true,
            })
        }
    
        if(modal === "signup") {
            setModalState({
                signUpModal: true,
                signInModal: false,
            })
        }

        if(modal === "close") {
            setModalState({
                signUpModal: false,
                signInModal: false,
            })
        }
    };
    
    return(
        <UserContext.Provider value={{modalState, toggleModals, signUp, currentUser, signIn}}>
            {!loadingData && children}
        </UserContext.Provider>
    );
}

export {
    UserContextProvider
}