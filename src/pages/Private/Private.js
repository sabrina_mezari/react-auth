import { useContext } from "react";
import { Outlet, useLocation, Navigate } from "react-router-dom";

import {UserContext} from "../../context/userContext";

function Private() {

    const { currentUser } = useContext(UserContext);
    console.log("Private", currentUser);

    if(!currentUser) {
        return <Navigate to="/"/>
    }

    return (
        <div className="container">
            <Outlet />
        </div>
    )
}

export default Private;
