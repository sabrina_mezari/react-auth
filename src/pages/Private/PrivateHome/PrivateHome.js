import dog from "./dog.gif";

function PrivateHome() {

    return (
        <div className="container p-5">
            <h1 className="display-3 text-light mb-4">
                Home sweet private home
            </h1>
            <img src={dog} alt="dog.gif"/>
        </div>
    )
}

export default PrivateHome;
