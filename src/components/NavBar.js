import { Link } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "../context/userContext";
import { signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { auth } from "../firebase-config";

function NavBar() {

    const {toggleModals} = useContext(UserContext);

    const navigate = useNavigate();

    const logOut = async() => {

        try {
            await signOut(auth)
            navigate("/")

        } catch {
            alert("Fore some reasons we can't deconnect, please check your internet connexion and retry.")
        }
    }

    return (
        <nav className="navbar navbar-light bg-light px-4">
            <Link to="/" className="navbar-brand">
                AuthJS
            </Link>
            <div>
                <button 
                onClick={() => toggleModals("signup")}
                className=" btn btn-primary">
                    Sign up
                </button>
                <button 
                onClick={() => toggleModals("signin")}
                className=" btn btn-primary ms-2">
                    Sign in
                </button>
                <button 
                onClick={logOut}
                className=" btn btn-danger ms-2">
                    Log out
                </button>
            
            </div>
        </nav>
        
    );
}

export default NavBar;